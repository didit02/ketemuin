import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Comingsoon from '../views/Comingsoon.vue';
import Dashboard from '../views/Dashboard.vue';
import How from '../views/Howitwork.vue';
import Contact from '../views/Contactus.vue';
import Pagenotfound from '../views/Pagenotfound.vue';
import Aboutus from '../views/Aboutus.vue';
import Beourclient from '../views/Beourclient.vue';
import Beourpartner from '../views/Beourpartner.vue';
import Faq from '../views/Faq.vue';
import Privacypolice from '../views/Privacypolice.vue';
import Termsandcondition from '../views/Termsandcondition.vue';
Vue.use(VueRouter);
const routes = [
  {
    path: '/', component: Dashboard,
    children: [
      { path: '/', redirect:'/comingsoon'},
      {
        path: '/id',
        component: Home
      },
      {
        path: '/id/cara-kerja',
        name: 'how',
        component: How
      },
      {
        path: '/id/hubungi-kami',
        name: 'contact',
        component: Contact
      },
      {
        path: '/id/tentang-kami',
        name: 'about',
        component: Aboutus
      },
      {
        path: '/id/klien',
        name: 'klien',
        component: Beourclient
      },
      {
        path: '/id/mitra',
        name: 'mitra',
        component: Beourpartner
      },
      {
        path: '/en',
        component: Home
      },
      {
        path: '/en/how-it-work',
        name: 'how',
        component: How
      },
      {
        path: '/en/contact-us',
        name: 'contact',
        component: Contact
      },
      {
        path: '/en/about-us',
        name: 'about',
        component: Aboutus
      },
      {
        path: '/en/client',
        name: 'klien',
        component: Beourclient
      },
      {
        path: '/en/partner',
        name: 'mitra',
        component: Beourpartner
      },
      {
        path: '/id/faq',
        name: 'faq',
        component: Faq
      },
      {
        path: '/en/faq',
        name: 'faq',
        component: Faq
      },
    ]
  },
    {
    path: '/comingsoon',
    name: 'comingsoon',
    component: Comingsoon
  },
  {
    path: '/id/privasi',
    name: 'privasi',
    component: Privacypolice
  },
  {
    path: '/en/privacy',
    name: 'privacy',
    component: Privacypolice
  },
  {
    path: '/en/tnc',
    name: 'tnc',
    component: Termsandcondition
  },
  {
    path: '/id/snk',
    name: 'snk',
    component: Termsandcondition
  },
  {
    path: "/404",
    name: '404',
    component: Pagenotfound
  },
  {
    path: "**",
    redirect:'/404'
  },
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
